import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 25, top: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Hello",
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.black38)),
                Text("Akhmad Zulkarnain",
                    style: GoogleFonts.openSans(
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        color: Colors.black54)),
              ],
            ),
          ),
          Container(
            height: 39,
            margin: EdgeInsets.only(left: 25, right: 25, top: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(21), color: Colors.black12),
            child: Stack(
              children: <Widget>[
                TextField(
                  // maxLengthEnforced: true,
                  style: GoogleFonts.openSans(
                      fontSize: 12,
                      color: Colors.black26,
                      fontWeight: FontWeight.w600),
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.only(left: 19, right: 50, bottom: 8),
                      border: InputBorder.none,
                      hintText: "Search Materi",
                      hintStyle: GoogleFonts.openSans(
                          fontSize: 12,
                          color: Colors.black45,
                          fontWeight: FontWeight.w600)),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 25, right: 17, top: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Rekomended",
                  style: GoogleFonts.openSans(
                      color: Colors.black45, fontWeight: FontWeight.w700),
                ),
                Text("See all")
              ],
            ),
          ),
          Container(
            height: 250,
            padding: EdgeInsets.only(left: 25, right: 21, top: 11),
            // color: Colors.black12,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisSize: MainAxisSize.min,
                  // mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Card(
                      elevation: 11.0,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg',
                        fit: BoxFit.cover,
                        height: 135.0,
                        width: 135.0,
                      ),
                      clipBehavior: Clip.antiAlias,
                      // margin: EdgeInsets.all(8.0),
                    ),
                    Text(
                      " Javascript",
                      style: GoogleFonts.openSans(
                          color: Colors.black45, fontWeight: FontWeight.w700),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                      ],
                    ),
                    Container(
                      width: 135,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Rp.100",
                            style: GoogleFonts.openSans(
                                color: Colors.black45,
                                fontWeight: FontWeight.w700),
                          ),
                          ElevatedButton(onPressed: () {}, child: Text('Buy'))
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Card(
                      elevation: 18.0,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg',
                        fit: BoxFit.cover,
                        height: 135.0,
                        width: 129.0,
                      ),
                      clipBehavior: Clip.antiAlias,
                      // margin: EdgeInsets.all(8.0),
                    ),
                    Text(
                      " Golang",
                      style: GoogleFonts.openSans(
                          color: Colors.black45, fontWeight: FontWeight.w700),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                      ],
                    ),
                    Container(
                      width: 135,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            " Rp.100",
                            style: GoogleFonts.openSans(
                                color: Colors.black45,
                                fontWeight: FontWeight.w700),
                          ),
                          ElevatedButton(onPressed: () {}, child: Text('Buy'))
                        ],
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Card(
                      elevation: 18.0,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg',
                        fit: BoxFit.cover,
                        height: 135.0,
                        width: 129.0,
                      ),
                      clipBehavior: Clip.antiAlias,
                      // margin: EdgeInsets.all(8.0),
                    ),
                    Text(
                      " Pyton",
                      style: GoogleFonts.openSans(
                          color: Colors.black45, fontWeight: FontWeight.w700),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                        Icon(
                          Icons.star,
                          color: Colors.yellow,
                          size: 24.0,
                        ),
                      ],
                    ),
                    Container(
                      width: 135,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            " Rp.100",
                            style: GoogleFonts.openSans(
                                color: Colors.black45,
                                fontWeight: FontWeight.w700),
                          ),
                          ElevatedButton(onPressed: () {}, child: Text('Buy'))
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 25, right: 17, top: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Mentor",
                  style: GoogleFonts.openSans(
                      color: Colors.black45, fontWeight: FontWeight.w700),
                ),
                Text("See all")
              ],
            ),
          ),
          Container(
            height: 135,
            padding: EdgeInsets.only(left: 25, right: 21, top: 11, bottom: 31),
            // color: Colors.black12,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                        'https://media-cdn.tripadvisor.com/media/photo-s/0d/7c/59/70/farmhouse-lembang.jpg'),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
