import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:learning_path/layouts/home.dart';
import 'package:learning_path/layouts/profil.dart';
import 'package:learning_path/layouts/class.dart';
import 'package:learning_path/layouts/progress.dart';

class Nafigator extends StatefulWidget {
  @override
  _NafigatorState createState() => _NafigatorState();
}

class _NafigatorState extends State<Nafigator> {
  int selectedPage = 0;
  final _pageOptions = [Home(), Class(), Progress(), Profile()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOptions[selectedPage],
      bottomNavigationBar: ConvexAppBar(
        items: [
          TabItem(icon: Icons.home, title: 'Home'),
          TabItem(icon: Icons.favorite, title: 'Class'),
          TabItem(icon: Icons.map, title: 'Progress'),
          TabItem(icon: Icons.person, title: 'Profile'),
        ],
        initialActiveIndex: 0, //optional, default as 0
        onTap: (int i) {
          setState(() {
            selectedPage = i;
          });
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.

      // initialRoute: "/",
      // routes: {
      //   "/": (_) => MainScreen(),
      //   "/bar": (BuildContext context) => MainScreen(),
      //   "/custom": (BuildContext context) => CustomAppBarDemo(),
      //   "/fab": (BuildContext context) => ConvexButtonDemo(),
      // },
    );
  }
}
